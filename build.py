#!/usr/bin/python3

import shutil
import os

MAIN_FILE = 'main.tex'
CONFIGURATIONS = [
]

def substitute_data(configuration: tuple):
	with open(MAIN_FILE, 'r') as main_file:
		data = main_file.read()
		data = data.replace('% #config', '\\input{%s}' % configuration[0])
		data = data.replace('% #content', '\\input{%s}' % configuration[1])

    with open(MAIN_FILE, 'w') as output:
        output.write(data)

def run():
    backup_filename = f'{MAIN_FILE}.bak'
    shutil.copyfile(MAIN_FILE, backup_filename)
    if not os.path.exists('dist'):
        os.mkdir('dist')
    for index, configuration in enumerate(CONFIGURATIONS):
        substitute_data(configuration)
        os.system('make build')
        shutil.move('main.pdf', f'dist/{configuration[2]}.pdf')
        shutil.move(backup_filename, MAIN_FILE)

if __name__ == '__main__':
    run()
