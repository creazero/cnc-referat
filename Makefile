all: build_all

build_all:
	python3 build.py

build:
	latexmk -cd -f -xelatex -interaction=nonstopmode --shell-escape -synctex=1 main.tex 

clean:
	rm -r _minted-main/ dist/
	latexmk -C
